import React from 'react'
import User from './views/User'
import Login from './views/Login'
import UserCreate from './components/user/create'

import {
  BrowserRouter as Router,
  Switch,
  Route, 
  //Redirect

} from "react-router-dom";

//$Recycle.Binimport { isAuthenticated } from './config/auth'

const CustomRoute = ({ ...rest }) => {
    // if(isAuthenticated()) {
    //     return <Redirect to='/debt-list' />
    // }
    return <Route { ...rest } />
}


const Routers = () => (
  <Router>
    <Switch>
        <Route exact path={"/"} component={Login}/> 
        <Route exact path={"/create-user"} component={UserCreate}/>
        <CustomRoute path="/" component={User}/> 
    </Switch>
  </Router>
  
)

export default Routers