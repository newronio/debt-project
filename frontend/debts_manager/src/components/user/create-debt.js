import React, {useEffect, useState} from 'react'
import { CreateDebt, EditDebt } from '../../services/debts.js'
import './debt.css'
import Alert from '../alert/index.js'
import Nav from '../../components/layout/nav/nav'
import{ useHistory, useParams } from 'react-router-dom'

//import Loading from '../loading/loading'

const Create = (props) => {

const [form, setForm] = useState({})   
const [isEdit, setisEdit] = useState(false)
const [isSubmit, setSubmit] = useState(false) 
const [alert, setAlert] = useState({})
const history = useHistory()
const { id } = useParams()

useEffect(() => {

    const getShowUser = async () => {
        const debt = await EditDebt(id)

        // if(user.data.senha) {        Habilitar na criação de usuário
        //     delete user.data.senha
        // }

        setForm(debt.data)
        //console.log(debt)
    }
    
    if(id) {
        setisEdit(true)
        getShowUser()
    }

}, [id])


const handleChange = (event) => {
    setForm({
        ...form,
        [event.target.name]: event.target.value 
    })
    return;
}

const formIsValid = () => {
    return form.typeDebt && form.debtLocation && form.debtCost && form.date
}

const submitForm = async () => {

    try{
        setSubmit(true)

        await CreateDebt(form)

        setAlert({
            type: 'success',
            message: 'Cadastrado com sucesso',
            show: true
        })

        setSubmit(false)
         history.push('/debt-list')
        

    }catch (err) {
        setAlert({
            type: 'error',
            message: 'Ocorreu um erro',
            show: true
        })
    }
    
}

    return (
        <section >
        <Nav name="Lista" to="/debt-list"/>
            <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false}/>

            <div className="create_debt">
                <div className="form_login">
                        <div>
                            <label htmlFor="auth_debt">Tipo de débito</label>
                            <input disabled={isSubmit} type="name" id="typeDebt" name="typeDebt" placeholder="Insira o o tipo de débito" onChange={handleChange} value={form.typeDebt || ""}/>
                        </div>

                        <div>
                            <label htmlFor="auth_debt">Localização do Débito</label>
                            <input disabled={isSubmit} type="name" id="debtLocation" name="debtLocation" placeholder="Insira a localização do débito" onChange={handleChange} value={form.debtLocation || ""}/>
                        </div>

                        <div>
                            <label htmlFor="auth_cost">Custo</label>
                            <input disabled={isSubmit} type="name" id="debtCost" name="debtCost" placeholder="Insira o valor do custo" onChange={handleChange} value={form.debtCost || ""}/>
                        </div>
                        <div>
                            <label htmlFor="auth_date">Data</label>
                            <input disabled={isSubmit} type="date" id="date" name="date" onChange={handleChange} value={form.date || ""}/>
                        </div>
                    <button disabled={!formIsValid()} onClick={submitForm}>
                        {isEdit ? "Atualizar" : "Cadastrar"}
                    </button>
                    {/* <Loading show={isSubmit}/> */}
                    {isSubmit ? <div>Carregando...</div> : ""}
                </div>
            </div>
        </section>
    )
}

export default Create
