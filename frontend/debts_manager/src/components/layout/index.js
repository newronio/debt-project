import React from 'react'

import Header from './header/Header'
//import Menu from './components/layout/nav/nav'
import Footer from './footer/Footer'



const index = (props) => {
    //console.log(props)
    return (
        <div>
            <Header {...props} title="GERENCIADOR DE DÉBITOS"/>
                <main>
                    {props.children}
                </main>
            <Footer/>
        </div>
       
    )
}

export default index
