import React, { useEffect, useState } from 'react'
import Layout from '../components/layout'
//import UserList from './components/user/list'
import DebtList from '../components/user/debt-list'
//import UserCreate from '../components/user/create'
import DebtCreate from '../components/user/create-debt'
import jwt from 'jsonwebtoken'
import { getToken } from '../config/auth'


import {
    Switch,
    Route,
  } from "react-router-dom";

  
const User = (props) => {
const [userInfo, setUserInfo] = useState({})

useEffect(() => {
    (async () => {
        const { user} = await jwt.decode(getToken())
        setUserInfo(user)
    })()
    return () =>  {  }
}, [])


    return (
        <Layout info={userInfo}>
            <Switch>  
                <Route exact path={props.match.path + "debt-list"} component={DebtList}/>
                <Route exact path={props.match.path + "create-debt"} component={DebtCreate}/>
                <Route exact path={props.match.path + "edit/:id"} component={DebtCreate}/>
                <Route exact path="*" component={() => (<h1>404 | Not Found</h1>)}/>      
            </Switch>
        </Layout>
    )
}

export default User
