const { clientHttp } = require("../config/config");

const authentication = (token) => clientHttp.post('/auth', token)

export {
    authentication
}