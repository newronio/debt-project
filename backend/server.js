const express = require('express');
const cors = require('cors');
const connectDB = require('./config/db');
const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

connectDB()

const userRouter = require('./routes/users.routes')
const debtRouter = require('./routes/debt.routes')
const authRouter = require('./routes/auth.routes')

app.use('/users', userRouter);
app.use('/debts', debtRouter);
app.use('/auth', authRouter ) 


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));