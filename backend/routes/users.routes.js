const express = require('express');
let User = require('../models/user.model');
const { check, validationResult } = require('express-validator');
const app = express();
const bcrypt = require('bcryptjs')  
const auth = require('../middleaware/auth')

app.get('/', auth, async (req, res, next) => {

    try{
        const user = await User.find()
        res.json(user)
        
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server Error"})
    }
    // User.find()
    // .then(users => res.json(users))
    // .catch(err => res.status(400).json('Error: ' + err))
})

app.get('/:id', auth, async (req, res, next) => {

    try{
        const id = req.params.id
        
        const userId =  await User.findById(id);
        res.send(userId)

    }catch(err){
        res.status(500).send({"error": "Server Error"})
        console.log(err)
    }
    // User.find()
    // .then(users => res.json(users))
    // .catch(err => res.status(400).json('Error: ' + err))
})

app.post('/', [
    check('name', 'O Campo precisa ser preenchido').not().isEmpty(),
    check('email', 'Coloque um email válido').isEmail(),
    check('password', 'A senha precisa conter mais que 5 dígitos').isLength({min: 5})
], async (req, res) => {

    try{
        let { username, name, lastname, email, password, is_active, is_admin } = req.body       
         
        const users = new User({
            username, 
            name, 
            lastname, 
            email, 
            password, 
            is_active, 
            is_admin
        })
            
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }

        const salt = await bcrypt.genSalt(10)  
        users.password = await bcrypt.hash(password, salt)

        await users.save()
        
        res.json(users)
        console.log(users)

    }catch(error){ 
        console.error(error)
        res.status(500).send({"error": "Server Error"})
    }

    // .then(() => res.json('Usuario adicionado'))
    // .catch(err => res.status(400).json('Error: ' + err))
})

app.post('/:id',  [
    check('nome').not().isEmpty(),
    check('email').isEmail(),
    check('password').isLength({min: 5})
], async (req, res) => {

    try{
        
        const id = req.params.id
        
        let bodyRequest = req.body

        const update = { $set: bodyRequest}

        const user = await User.findByIdAndUpdate (id, update, {new: true})
        let salt = 10

        if (!user){
            res.status(404).send({"error": "user not exist"})
        }
            user.password = await bcrypt.hash(bodyRequest.password, salt)
            res.json(user)
        
    }catch(error){ 
        console.error(error.message)
        res.status(500).send({"error": "Server Error"})
    }

    // .then(() => res.json('Usuario adicionado'))
    // .catch(err => res.status(400).json('Error: ' + err))
})

app.delete('/:id', auth, async (req, res, next) => {

    try{
        const id = req.params.id
        const user = await User.findByIdAndDelete(id)

        if(user){
            res.json(user)
        }

        res.status(500).send({"error": "User not found"})
        
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server Error"})
    }
        // User.find()
        // .then(users => res.json(users))
        // .catch(err => res.status(400).json('Error: ' + err))
})


module.exports = app;