const express = require('express')
const User = require('../models/user.model')
const router = express.Router();
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const {check, validationResult} = require('express-validator')

//@route GET/auth/
//@desc Authenticate user & get token
//@access Public

router.post('/',[
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'password is required').exists()
  ], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }
    const { email, password } = req.body

    try{
        let user = await User.findOne({ email }).select('id password email is_active is_admin name')
        if (!user) {
            return res.status(404).json({ errors: [{ msg: 'Usuário não existe' }] })
        }else{
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({ errors: [{ msg: 'Senha Incorreta' }] });
            }else{
                if (user.is_active == false){
                  return res.status(403).json({ errors: [{ msg: 'user is not active' }] });
                }
                const payload = {
                    user: {
                      id: user.id,
                      name: user.name,
                      is_active: user.is_active,
                      is_admin: user.is_admin
                    }
                }


                jwt.sign( payload, config.get('jwtSecret'), { expiresIn: '5 days' },
                    (err, token) => {
                      if (err) throw err;
                      payload.token = token
                      res.json(payload);
                    }
                  );
            }
        }

    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }

})

module.exports = router;
