const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const debtSchema = new Schema({
   
    typeDebt : {
        type: String,
        required: true,
    },
    debtLocation : {
        type: String,
        required: true
    },
    debtCost : {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }, 
    img: { 
        data: Buffer, 
        type: String
     },
     date: {
        type: Date,
        default: Date.now
    }, 
}, {
    timestamps: true,
})

module.exports = mongoose.model('debts', debtSchema);